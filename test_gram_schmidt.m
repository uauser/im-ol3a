
% Gram-Schmidt-orthonormalisatie
N=10;
[A,b] = getPoissonMatrix( N );
x_0=rand(N,1);
r=b-A*x_0;
k=3;
v=zeros(N,k);
v(:,1)=r/norm(r,2)
for i=1:(k-1)
     som=0;
     w=A*v(:,i)
     for j=1:i
         som=som+(w'*v(:,j))*v(:,j);
     end
     v(:,i+1)=w-som;
     v(:,i+1)=v(:,i+1)/norm(v(:,i+1),2);
end
diag(v'*v)
%%
% Gestabiliseerde Gram-Schmidt-orthonormalisatie (cursus algoritme 6)
v2=zeros(N,k);
v2(:,1)=r/norm(r,2);
for i=1:(k-1)
      z = A*v2(:,i);
      for j=1:i
          z = z - (z'*v2(:,j))*v2(:,j);
      end
      v2(:,i+1)=z/norm(z,2);
end
diag(v2'*v2)
