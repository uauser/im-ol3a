
% Gram-Schmidt-orthonormalisatie
delta =10^(-7);
A = [1 1 1; delta delta 0; delta 0 delta];
v = zeros(3,3);
v(:,1)=A(:,1)/norm(A(:,1),2);
k=3;
for i=2:k
     som=0;
     for j=1:(i-1)
         som=som+(A(:,i)'*v(:,j))*v(:,j);
     end
     v(:,i)=A(:,i)-som;
     v(:,i)=v(:,i)/norm(v(:,i),2)
end

%%
% Gestabiliseerde Gram-Schmidt-orthonormalisatie

v2 = zeros(3,3);
v2(:,1)=A(:,1)/norm(A(:,1),2);
for i=2:k
      v2(:,i) = A(:,i);
      for j=1:(i-1)
          v2(:,i) = v2(:,i) - (v2(:,i)'*v2(:,j))*v2(:,j);
      end
      v2(:,i)=v2(:,i)/norm(v2(:,i),2);
end
